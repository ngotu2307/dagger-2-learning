# Dagger 2

## Add Dependency

```
def daggerVersion = "2.45"
implementation "com.google.dagger:dagger:$daggerVersion"
kapt "com.google.dagger:dagger-compiler:$daggerVersion"
```

## Diagram
https://drive.google.com/file/d/1ECbYwZyAXe6x9BDmYslL--UPQ9Er7Mse/view?usp=share_link
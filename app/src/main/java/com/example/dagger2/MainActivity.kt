package com.example.dagger2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.dagger2.car.Car
import com.example.dagger2.di.DaggerAppComponent
import com.example.dagger2.di.DieselEngineModule
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var car1: Car

    @Inject
    lateinit var car2: Car

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val appComponent = DaggerAppComponent.builder()
            .horsePower(100)
            .engineCapacity(1400)
            .build()
        appComponent.inject(this)
        car1.drive()
        car2.drive()
    }
}
package com.example.dagger2.car

import android.util.Log
import javax.inject.Inject

class Car @Inject constructor(
    private val wheels: Wheels,
    private val engine: Engine,
    private val driver: Driver
) {
    companion object {
        private const val TAG = "Car"
    }

    fun drive() {
        engine.start()
        Log.d(TAG, "$driver drivers $this")
    }

    @Inject
    fun enableRemote(remote: Remote) {
        remote.setListener(this)
    }
}
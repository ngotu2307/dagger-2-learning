package com.example.dagger2.car

import android.util.Log
import javax.inject.Inject

class DieselEngine @Inject constructor(
    private var horsePower: Int
): Engine {
    companion object {
        private const val TAG = "Car"
    }

    override fun start() {
        Log.d(TAG, "Diesel engine started. Horsepower: $horsePower ")
    }
}
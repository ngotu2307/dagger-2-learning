package com.example.dagger2.car

// interface have no constructor
interface Engine {
    fun start()
}
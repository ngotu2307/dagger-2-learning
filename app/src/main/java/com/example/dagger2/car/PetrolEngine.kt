package com.example.dagger2.car

import android.util.Log
import javax.inject.Inject
import javax.inject.Named

class PetrolEngine @Inject constructor(
    @Named("horsePower") private var horsePower: Int,
    @Named("engineCapacity") private val engineCapacity: Int
) : Engine {
    companion object {
        private const val TAG = "Car"
    }

    override fun start() {
        Log.d(TAG, "Petrol engine started. Horsepower: $horsePower, engineCapacity: $engineCapacity")
    }
}
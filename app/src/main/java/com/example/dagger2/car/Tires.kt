package com.example.dagger2.car

import android.util.Log

class Tires {
    // we don't own this class so we can't annotate it with @Inject
    companion object {
        private const val TAG = "Car"
    }

    fun inflate() {
        Log.d(TAG, "Tires inflated")
    }
}
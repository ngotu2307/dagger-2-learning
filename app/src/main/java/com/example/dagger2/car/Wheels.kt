package com.example.dagger2.car

class Wheels constructor(
    private var rims: Rims,
    private var tires: Tires
){
    companion object {
        private const val TAG = "Car"
    }
}
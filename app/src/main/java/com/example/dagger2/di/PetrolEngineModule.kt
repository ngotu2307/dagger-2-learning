package com.example.dagger2.di

import com.example.dagger2.car.Engine
import com.example.dagger2.car.PetrolEngine
import dagger.Module
import dagger.Provides

@Module
class PetrolEngineModule {

    @Provides
    fun provideEngine(engine: PetrolEngine): Engine {
        return engine
    }
}